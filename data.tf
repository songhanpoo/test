data "external" "lookupDataSrc" {
  program = ["python3","./externals/helper.py"]
  query = {
      lstMigrate = jsonencode(local.lstMigrate)
      provider = jsonencode(var.PROVIDER_URL)
  }
}

locals {
  lookup_env = lower((split("-", terraform.workspace))[2])
  # lookup_host_from = lookup(lookup(var.PROVIDER_URL, local.lookup_env, null),"host",null)
  # lookup_port_from = lookup(lookup(var.PROVIDER_URL, local.lookup_env, null),"port",null)
  # lookup_auth_from = lookup(lookup(var.PROVIDER_URL, local.lookup_env, null),"auth",null)
  # lookup_env_dest = "${ lookup_env == "dev" ? "sit" : ( lookup_env == "sit" ? "uat" : "prod" )}"
  # lookup_host_to = lookup(lookup(var.PROVIDER_URL, local.lookup_env_dest, null),"host",null)
  # lookup_port_to = lookup(lookup(var.PROVIDER_URL, local.lookup_env_dest, null),"port",null)
  # lookup_auth_to = lookup(lookup(var.PROVIDER_URL, local.lookup_env_dest, null),"auth",null)

  lstMigrate = lookup(var.lstMigrate, local.lookup_env, null)

  idDashboardFrom   = [
    for item in jsondecode(data.external.lookupDataSrc.result.data) : tostring(item.idDashboard)
  ]
}

output "debuggertest" {
  value = local.idDashboardFrom
  depends_on = [
    data.external.lookupDataSrc,
  ]
}

output "lookupDataSrc" {
  value = data.external.lookupDataSrc.result
  depends_on = [
    data.external.lookupDataSrc
  ]
}

data "grafana_dashboard" "from_id" {
  provider = grafana.from
  for_each = toset(local.idDashboardFrom)
  dashboard_id = each.key
  depends_on = [
    local.idDashboardFrom
  ]
}

resource "local_file" "foo" {
  for_each = data.grafana_dashboard.from_id
  content  = each.value.config_json
  filename = "${path.module}/templates/config-${each.key}.json"
  depends_on = [
    data.grafana_dashboard.from_id
  ]
}

data "external" "transDataSrc" {
  # working_dir = "./externals"
  program = ["python3","./externals/main.py"]
  query = {
      # lookupDataSrc = jsonencode(data.external.lookupDataSrc.result)
      idDashboardFrom = jsonencode(local.idDashboardFrom)
      lstUidDataSourceFrom = data.external.lookupDataSrc.result.lstUidDataSourceFrom
      lstUidDataSourceTo = data.external.lookupDataSrc.result.lstUidDataSourceTo
  }
  depends_on = [
    # data.external.lookupDataSrc,
    local_file.foo
  ]
}

output "transDataSrc" {
  value = data.external.transDataSrc.result
  depends_on = [
    data.external.transDataSrc
  ]
}


resource "grafana_dashboard" "metrics" {
  provider = grafana.to
  for_each = toset(local.idDashboardFrom)
  config_json = file("./templates/config-${each.key}.json")
  overwrite = true
  depends_on = [
    data.external.transDataSrc,
  ]
}

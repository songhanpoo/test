provider "grafana" {
  alias = "from"
  url   = "http://${var.PROVIDER_URL.test.host}:${var.PROVIDER_URL.test.port}"
  auth  = "${var.PROVIDER_URL.test.auth}"
}

provider "grafana" {
  alias = "to"
  url   = "http://${var.PROVIDER_URL.uat.host}:${var.PROVIDER_URL.uat.port}"
  auth  = "${var.PROVIDER_URL.uat.auth}"
}

terraform {
  required_providers {
    grafana = {
      source = "grafana/grafana"
      version = "1.20.1"
    }
  }
}
variable "lstMigrate" {
  type = any
  default = {
    "test" = [{
        "from"          = "test"
        "withDashboard" = "Level 1 - Overall Monitoring Copy"
        "withDataSrc"   = ["Prometheus","sourcing_metadata"]
        "to"            = "uat"
      },
      {
        "from"          = "test"
        "withDashboard" = "Airflow cluster dashboard Copy"
        "withDataSrc"   = ["Prometheus","sourcing_metadata"]
        "to"            = "uat"
      }]
    "uat" = [{
        "from"          = "uat"
        "withDashboard" = "Level 1 - Overall Monitoring Copy"
        "withDataSrc"   = ["Prometheus","sourcing_metadata"]
        "to"            = "sit"
      },
      {
        "from"          = "uat"
        "withDashboard" = "Airflow cluster dashboard Copy"
        "withDataSrc"   = ["Prometheus","sourcing_metadata"]
        "to"            = "sit"
      }],
    }
}

variable "PROVIDER_URL" {
  description = ""
  type        = map(map(string))
  default     = {
    "test"  = {
        host      = "10.101.9.133"
        port      = "9889"
        auth     = "eyJrIjoiUkdXczNETDNmQU03a25pUUpzcFdMSFljUlRjV1BmRXMiLCJuIjoidGVzdCIsImlkIjoxfQ=="
      }
    "uat"  = {
        host      = "10.101.5.112"
        port      = "9889"
        auth     = "eyJrIjoidnF6bEwyV29LcHFFS204SFEwSnFpUE5aYjJ2NGtWRmEiLCJuIjoiVEVTVCIsImlkIjoxfQ=="
      }
  }
}
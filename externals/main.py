import json
import logging
from helper import fetch,replaceStr

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


if __name__ == "__main__":
  argv = fetch()
  lstUidDataSourceFrom = json.loads(argv.get("lstUidDataSourceFrom"))
  lstUidDataSourceTo = json.loads(argv.get("lstUidDataSourceTo"))
  idDashboardFrom = json.loads(argv.get("idDashboardFrom"))

  for i in range(len(lstUidDataSourceFrom)):
    for el in lstUidDataSourceFrom[i]:
      replaceStr(
        file="/Users/hieunlp/Desktop/DEV/datalake/itops/ds1-onpremise-monitoring/templates/config-{}.json".format(el),
        rpl_me=lstUidDataSourceFrom[i][el],
        new_str=lstUidDataSourceTo[i][el]
      )

  tmp = {
    'status':'ok',
    'idDashboardFrom': json.dumps(idDashboardFrom),
    "lstUidDataSourceFrom": json.dumps(lstUidDataSourceFrom),
    "lstUidDataSourceTo": json.dumps(lstUidDataSourceTo)
  }
  print(json.dumps(tmp))
  # print(os.environ)



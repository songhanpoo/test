import json
import requests
import logging
import os
import sys
# import dirtyjson


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

def getDataSource(url,token):
  LOGGER.info("This process for get all metadata datasource from- " + str(url))
  try:
    return requests.request(
          "GET", 
          "{}/api/datasources".format(url),
          headers={
            'content-type': "application/json",
            'authorization': "Bearer {}".format(token)
          }
        ).json()
  except:
    LOGGER.error("Error while reading the API Grafana - " + str(url))

def getDashboard(url,token,name):
  LOGGER.info("This process for get all metadata dashboard from- " + str(name))
  try:
    return requests.request(
          "GET", 
          "{}/api/search?query={}".format(url,name),
          headers={
            'content-type': "application/json",
            'authorization': "Bearer {}".format(token)
          }
        ).json()
  except:
    LOGGER.error("Error while reading the API Grafana - " + str(name))

def lookupDataSource(dataSrc,name):
  LOGGER.info("This process for lookup metadata datasource from- " + str(name))
  try:
    return [ i for i in dataSrc if i["name"] == name ]
  except:
    LOGGER.error("Error while lookup metadata order by - " + str(name))

def replaceStr(file,rpl_me,new_str):
  LOGGER.info("This process for lookup metadata datasource from- " + str(file))
  try:
    with open(file) as f:
      file_data = f.read()
    file_data = file_data.replace(rpl_me, new_str)

    with open(file, 'w') as f:
      f.write(file_data)
  except:
    LOGGER.error("Error while lookup metadata order by - " + str(file))

def writeFile(file,rpl_me,new_str):
  LOGGER.info("This process for lookup metadata datasource from- " + str(file))
  try:
    with open(file) as f:
      file_data = f.read()
    file_data = file_data.replace(rpl_me, new_str)

    with open(file, 'w') as f:
      f.write(file_data)
  except:
    LOGGER.error("Error while lookup metadata order by - " + str(file))
def return_result(argv,lstUidDataSourceFrom,lstUidDataSourceTo):
  result = {
    "data": json.dumps(argv),
    "lstUidDataSourceFrom": json.dumps(lstUidDataSourceFrom),
    "lstUidDataSourceTo": json.dumps(lstUidDataSourceTo)
  }
  print(json.dumps(result))
  

def fetch():
    try:
      return json.loads(sys.stdin.read())
    except ValueError as e:
        sys.exit(e)

if __name__ == "__main__":
  argv       = fetch()
  lstMigrate = json.loads(argv.get("lstMigrate"))
  provider   = json.loads(argv.get("provider"))

  obj                  = []
  lstUidDataSourceFrom = []
  lstUidDataSourceTo   = []

  for item in lstMigrate:
    if item["withDashboard"] != None:
      obj += [
        {
          "from"         : item["from"],
          "withDashboard": item["withDashboard"],
          "idDashboard"  : (getDashboard(
                              url= "http://{}:{}".format(
                                  provider[item["from"]]["host"],
                                  provider[item["from"]]["port"],
                                ),
                              token = provider[item["from"]]["auth"],
                              name  = item["withDashboard"]
                            ))[0]["id"],
          "withDataSrc"         : item["withDataSrc"],
            "to": item["to"]
        },
      ]


      tmp = (getDashboard(
                url= "http://{}:{}".format(
                    provider[item["from"]]["host"],
                    provider[item["from"]]["port"],
                  ),
                token = provider[item["from"]]["auth"],
                name  = item["withDashboard"]
              ))[0]["id"]

      for element in item["withDataSrc"]:
        lstUidDataSourceFrom += [
            {
              tmp : (lookupDataSource(
                      dataSrc=getDataSource(
                          url= "http://{}:{}".format(
                              provider[item["from"]]["host"],
                              provider[item["from"]]["port"],
                            ),
                          token = provider[item["from"]]["auth"],
                        ),
                      name=element))[0]["uid"]
          }]
        lstUidDataSourceTo += [
            {
              tmp : (lookupDataSource(
                      dataSrc=getDataSource(
                          url= "http://{}:{}".format(
                              provider[item["to"]]["host"],
                              provider[item["to"]]["port"],
                            ),
                          token = provider[item["to"]]["auth"],
                        ),
                      name=element))[0]["uid"]
          }]

  return_result(
      obj,
      lstUidDataSourceFrom,
      lstUidDataSourceTo
    )
    